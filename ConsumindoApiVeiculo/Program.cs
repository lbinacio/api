﻿using Newtonsoft.Json;
using System;
using System.Net;
using teste.Models;

namespace ConsumindoApiVeiculo
{
    class Program
    {
        static void Main(string[] args)
        {

            string url = "http://senacao.tk/objetos/veiculo_array_objeto";

           Carro Carro = BuscarCarros(url);

            Console.WriteLine("Carro");
            Console.WriteLine(
                String.Format("Marca: {0} - Modelo: {1} - Ano: {2} - Quilo: {3}",
                Carro.Marca, Carro.Modelo, Carro.Ano, Carro.Quilo));
            Console.WriteLine("\n\n");
            Console.WriteLine("Vendedor");
            Console.WriteLine(
                String.Format("Nome: {0} - Idade: {1} - Celular: {2} - Cidade: {3}",
                Carro.Vendedor.Nome, Carro.Vendedor.Idade, Carro.Vendedor.Celular, Carro.Vendedor.Cidade));
            Console.WriteLine("\n\n");
            Console.WriteLine("Opcionais");

            foreach (string Opcionais in Carro.Opcionais)
            {
                Console.WriteLine(Opcionais);
            }

            Console.ReadLine();
            //Console.WriteLine("Hello World!");
        }



        public static Carro BuscarCarros(string url)
        {
            WebClient wc = new WebClient();
            string content = wc.DownloadString(url);

            Console.WriteLine(content);
            Console.WriteLine("\n\n");

            Carro Carro = JsonConvert.DeserializeObject<Carro>(content);

            return Carro;


        }
    }
}
